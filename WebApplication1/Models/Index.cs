﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMI.MonitoringOracle.Models
{
    public class Index
    {
        public int OracleCount { get; set; }
        public List<Int_Batch_ProcessViewModel> ListBatch { get; set; }

        public class Int_Batch_ProcessViewModel
        {
            public string INT_SOURCE_ID { get; set; }
            public decimal INT_BATCH_ID { get; set; }
            public string SCHEDULER_FLAG { get; set; }
            public string REQUEST_PHASE { get; set; }
            public string REQUEST_STATUS { get; set; }
            public decimal? REQUEST_ID { get; set; }
            public string BATCH_FLAG { get; set; }
            public System.DateTime? CREATION_DATE { get; set; }
            public string CREATED_BY { get; set; }
            public System.DateTime? LAST_UPDATE_DATE { get; set; }

            public String LAST_UPDATE_BY { get; set; }

        }
    }
}