﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SMI.MonitoringOracle.Models
{
    public class IntValidationLogViewModel
    {
        [Display(Name = "Source ID")]
        public string INT_SOURCE_ID { get; set; }

        [Display(Name = "Batch ID")]
        public decimal INT_BATCH_ID { get; set; }

        [Display(Name = "Staging ID")]
        public decimal INT_STAGING_ID { get; set; }

        [Display(Name = "Log SEQ")]
        public decimal INT_LOG_SEQ { get; set; }

        [Display(Name = "Validation Log")]
        public string VALIDATION_LOG { get; set; }

        [Display(Name = "Created By")]
        public string CREATED_BY { get; set; }

        [Display(Name = "Creation Date")]
        public Nullable<System.DateTime> CREATION_DATE { get; set; }

        [Display(Name = "Last Updated By")]
        public string LAST_UPDATED_BY { get; set; }

        [Display(Name = "Last Update Date")]
        public Nullable<System.DateTime> LAST_UPDATE_DATE { get; set; }

        [Display(Name = "Request ID")]
        public Nullable<decimal> INT_REQUEST_ID { get; set; }
    }
}