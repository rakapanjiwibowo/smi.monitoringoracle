﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace SMI.MonitoringOracle.Service
{
    public class IntBatchProcessService
    {
        private OracleModel entities = new OracleModel();

        //Get all data book
        public IEnumerable<SMI_INT_VALIDATION_LOG> GetAllDataValidation()
        {
            return entities.SMI_INT_VALIDATION_LOG;
        }

        public SMI_INT_VALIDATION_LOG GetBook(string ID)
        {
            return entities.SMI_INT_VALIDATION_LOG.SingleOrDefault(x => x.INT_SOURCE_ID == ID);
        }

    }
}