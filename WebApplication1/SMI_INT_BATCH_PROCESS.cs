namespace SMI.MonitoringOracle
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APPS.SMI_INT_BATCH_PROCESS")]
    public partial class SMI_INT_BATCH_PROCESS
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(2)]
        public string INT_SOURCE_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public decimal INT_BATCH_ID { get; set; }

        [Required]
        [StringLength(1)]
        public string SCHEDULER_FLAG { get; set; }

        [StringLength(30)]
        public string REQUEST_PHASE { get; set; }

        [StringLength(30)]
        public string REQUEST_STATUS { get; set; }

        public decimal? REQUEST_ID { get; set; }

        [StringLength(1)]
        public string BATCH_FLAG { get; set; }

        public DateTime? CREATION_DATE { get; set; }

        [StringLength(100)]
        public string CREATED_BY { get; set; }

        public DateTime? LAST_UPDATE_DATE { get; set; }

        [StringLength(100)]
        public string LAST_UPDATED_BY { get; set; }

        public virtual SMI_INT_SOURCE SMI_INT_SOURCE { get; set; }
    }
}
