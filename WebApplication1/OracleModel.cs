namespace SMI.MonitoringOracle
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class OracleModel : DbContext
    {
        public OracleModel()
            : base("name=OracleModel")
        {
        }

        public virtual DbSet<SMI_INT_BATCH_PROCESS> SMI_INT_BATCH_PROCESS { get; set; }
        public virtual DbSet<SMI_INT_SOURCE> SMI_INT_SOURCE { get; set; }
        public virtual DbSet<SMI_INT_VALIDATION_LOG> SMI_INT_VALIDATION_LOG { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.INT_SOURCE_ID)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.INT_BATCH_ID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.SCHEDULER_FLAG)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.REQUEST_PHASE)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.REQUEST_STATUS)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.REQUEST_ID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.BATCH_FLAG)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.CREATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_BATCH_PROCESS>()
                .Property(e => e.LAST_UPDATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.INT_SOURCE_ID)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.INT_SOURCE_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.INT_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.MODUL)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.SOURCE_VALUE)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.PARAMETER1)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.PARAMETER2)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.PARAMETER3)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.PARAMETER4)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.PARAMETER5)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.CREATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .Property(e => e.LAST_UPDATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_SOURCE>()
                .HasMany(e => e.SMI_INT_BATCH_PROCESS)
                .WithRequired(e => e.SMI_INT_SOURCE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.INT_SOURCE_ID)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.INT_BATCH_ID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.INT_STAGING_ID)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.INT_LOG_SEQ)
                .HasPrecision(38, 0);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.VALIDATION_LOG)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.CREATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.LAST_UPDATED_BY)
                .IsUnicode(false);

            modelBuilder.Entity<SMI_INT_VALIDATION_LOG>()
                .Property(e => e.INT_REQUEST_ID)
                .HasPrecision(38, 0);
        }
    }
}
