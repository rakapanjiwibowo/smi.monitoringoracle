﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SMI.MonitoringOracle.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var viewModel = new Models.Index();
            //viewModel.OracleCount = 0;
            //viewModel.ListBatch = new List<Models.Index.Int_Batch_ProcessViewModel>();
            using (var context = new SMI.MonitoringOracle.OracleModel())
            {
                viewModel.OracleCount = context.SMI_INT_BATCH_PROCESS.Count();
                viewModel.ListBatch = context.SMI_INT_BATCH_PROCESS
                .Select(s => new Models.Index.Int_Batch_ProcessViewModel
                {
                    INT_BATCH_ID = s.INT_BATCH_ID,
                    INT_SOURCE_ID = s.INT_SOURCE_ID,
                    BATCH_FLAG = s.BATCH_FLAG,
                    SCHEDULER_FLAG = s.SCHEDULER_FLAG,
                    REQUEST_ID = s.REQUEST_ID,
                    REQUEST_PHASE =s.REQUEST_PHASE,
                    REQUEST_STATUS = s.REQUEST_STATUS,
                    CREATED_BY =s.CREATED_BY,
                    CREATION_DATE = s.CREATION_DATE
                    
                }).ToList();
            }
                return View(viewModel);
        }
    }
}