﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SMI.MonitoringOracle.Controllers
{
    public class SMI_INT_BATCH_PROCESSController : Controller
    {
        private OracleModel db = new OracleModel();

        // GET: SMI_INT_BATCH_PROCESS
        public ActionResult Index()
        {
            var sMI_INT_BATCH_PROCESS = db.SMI_INT_BATCH_PROCESS.Include(s => s.SMI_INT_SOURCE);
            return View(sMI_INT_BATCH_PROCESS.ToList());
        }

        // GET: SMI_INT_BATCH_PROCESS/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS = db.SMI_INT_BATCH_PROCESS.Find(id);
            if (sMI_INT_BATCH_PROCESS == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_BATCH_PROCESS);
        }

        // GET: SMI_INT_BATCH_PROCESS/Create
        public ActionResult Create()
        {
            ViewBag.INT_SOURCE_ID = new SelectList(db.SMI_INT_SOURCE, "INT_SOURCE_ID", "INT_SOURCE_NAME");
            return View();
        }

        // POST: SMI_INT_BATCH_PROCESS/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "INT_SOURCE_ID,INT_BATCH_ID,SCHEDULER_FLAG,REQUEST_PHASE,REQUEST_STATUS,REQUEST_ID,BATCH_FLAG,CREATION_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY")] SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS)
        {
            if (ModelState.IsValid)
            {
                db.SMI_INT_BATCH_PROCESS.Add(sMI_INT_BATCH_PROCESS);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.INT_SOURCE_ID = new SelectList(db.SMI_INT_SOURCE, "INT_SOURCE_ID", "INT_SOURCE_NAME", sMI_INT_BATCH_PROCESS.INT_SOURCE_ID);
            return View(sMI_INT_BATCH_PROCESS);
        }

        // GET: SMI_INT_BATCH_PROCESS/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS = db.SMI_INT_BATCH_PROCESS.Find(id);
            if (sMI_INT_BATCH_PROCESS == null)
            {
                return HttpNotFound();
            }
            ViewBag.INT_SOURCE_ID = new SelectList(db.SMI_INT_SOURCE, "INT_SOURCE_ID", "INT_SOURCE_NAME", sMI_INT_BATCH_PROCESS.INT_SOURCE_ID);
            return View(sMI_INT_BATCH_PROCESS);
        }

        // POST: SMI_INT_BATCH_PROCESS/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "INT_SOURCE_ID,INT_BATCH_ID,SCHEDULER_FLAG,REQUEST_PHASE,REQUEST_STATUS,REQUEST_ID,BATCH_FLAG,CREATION_DATE,CREATED_BY,LAST_UPDATE_DATE,LAST_UPDATED_BY")] SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sMI_INT_BATCH_PROCESS).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.INT_SOURCE_ID = new SelectList(db.SMI_INT_SOURCE, "INT_SOURCE_ID", "INT_SOURCE_NAME", sMI_INT_BATCH_PROCESS.INT_SOURCE_ID);
            return View(sMI_INT_BATCH_PROCESS);
        }

        // GET: SMI_INT_BATCH_PROCESS/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS = db.SMI_INT_BATCH_PROCESS.Find(id);
            if (sMI_INT_BATCH_PROCESS == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_BATCH_PROCESS);
        }

        // POST: SMI_INT_BATCH_PROCESS/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            SMI_INT_BATCH_PROCESS sMI_INT_BATCH_PROCESS = db.SMI_INT_BATCH_PROCESS.Find(id);
            db.SMI_INT_BATCH_PROCESS.Remove(sMI_INT_BATCH_PROCESS);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
