﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMI.MonitoringOracle;

namespace WebApplication1.Controllers
{
    public class SMI_INT_VALIDATION_LOGController : Controller
    {
        private OracleModel db = new OracleModel();

        // GET: SMI_INT_VALIDATION_LOG
        public async Task<ActionResult> Index()
        {
            return View(await db.SMI_INT_VALIDATION_LOG.ToListAsync());
        }

        // GET: SMI_INT_VALIDATION_LOG/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG = await db.SMI_INT_VALIDATION_LOG.FindAsync(id);
            if (sMI_INT_VALIDATION_LOG == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_VALIDATION_LOG);
        }

        // GET: SMI_INT_VALIDATION_LOG/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SMI_INT_VALIDATION_LOG/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "INT_SOURCE_ID,INT_BATCH_ID,INT_STAGING_ID,INT_LOG_SEQ,VALIDATION_LOG,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,INT_REQUEST_ID")] SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG)
        {
            if (ModelState.IsValid)
            {
                db.SMI_INT_VALIDATION_LOG.Add(sMI_INT_VALIDATION_LOG);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sMI_INT_VALIDATION_LOG);
        }

        // GET: SMI_INT_VALIDATION_LOG/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG = await db.SMI_INT_VALIDATION_LOG.FindAsync(id);
            if (sMI_INT_VALIDATION_LOG == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_VALIDATION_LOG);
        }

        // POST: SMI_INT_VALIDATION_LOG/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "INT_SOURCE_ID,INT_BATCH_ID,INT_STAGING_ID,INT_LOG_SEQ,VALIDATION_LOG,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,INT_REQUEST_ID")] SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sMI_INT_VALIDATION_LOG).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sMI_INT_VALIDATION_LOG);
        }

        // GET: SMI_INT_VALIDATION_LOG/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG = await db.SMI_INT_VALIDATION_LOG.FindAsync(id);
            if (sMI_INT_VALIDATION_LOG == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_VALIDATION_LOG);
        }

        // POST: SMI_INT_VALIDATION_LOG/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SMI_INT_VALIDATION_LOG sMI_INT_VALIDATION_LOG = await db.SMI_INT_VALIDATION_LOG.FindAsync(id);
            db.SMI_INT_VALIDATION_LOG.Remove(sMI_INT_VALIDATION_LOG);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
