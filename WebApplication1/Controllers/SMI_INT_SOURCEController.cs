﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SMI.MonitoringOracle;

namespace WebApplication1.Controllers
{
    public class SMI_INT_SOURCEController : Controller
    {
        private OracleModel db = new OracleModel();

        // GET: SMI_INT_SOURCE
        public async Task<ActionResult> Index()
        {
            return View(await db.SMI_INT_SOURCE.ToListAsync());
        }

        // GET: SMI_INT_SOURCE/Details/5
        public async Task<ActionResult> Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_SOURCE sMI_INT_SOURCE = await db.SMI_INT_SOURCE.FindAsync(id);
            if (sMI_INT_SOURCE == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_SOURCE);
        }

        // GET: SMI_INT_SOURCE/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SMI_INT_SOURCE/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "INT_SOURCE_ID,INT_SOURCE_NAME,INT_NAME,MODUL,SOURCE_VALUE,PARAMETER1,PARAMETER2,PARAMETER3,PARAMETER4,PARAMETER5,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE")] SMI_INT_SOURCE sMI_INT_SOURCE)
        {
            if (ModelState.IsValid)
            {
                db.SMI_INT_SOURCE.Add(sMI_INT_SOURCE);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sMI_INT_SOURCE);
        }

        // GET: SMI_INT_SOURCE/Edit/5
        public async Task<ActionResult> Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_SOURCE sMI_INT_SOURCE = await db.SMI_INT_SOURCE.FindAsync(id);
            if (sMI_INT_SOURCE == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_SOURCE);
        }

        // POST: SMI_INT_SOURCE/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "INT_SOURCE_ID,INT_SOURCE_NAME,INT_NAME,MODUL,SOURCE_VALUE,PARAMETER1,PARAMETER2,PARAMETER3,PARAMETER4,PARAMETER5,CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE")] SMI_INT_SOURCE sMI_INT_SOURCE)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sMI_INT_SOURCE).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sMI_INT_SOURCE);
        }

        // GET: SMI_INT_SOURCE/Delete/5
        public async Task<ActionResult> Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SMI_INT_SOURCE sMI_INT_SOURCE = await db.SMI_INT_SOURCE.FindAsync(id);
            if (sMI_INT_SOURCE == null)
            {
                return HttpNotFound();
            }
            return View(sMI_INT_SOURCE);
        }

        // POST: SMI_INT_SOURCE/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(string id)
        {
            SMI_INT_SOURCE sMI_INT_SOURCE = await db.SMI_INT_SOURCE.FindAsync(id);
            db.SMI_INT_SOURCE.Remove(sMI_INT_SOURCE);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
