namespace SMI.MonitoringOracle
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APPS.SMI_INT_SOURCE")]
    public partial class SMI_INT_SOURCE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SMI_INT_SOURCE()
        {
            SMI_INT_BATCH_PROCESS = new HashSet<SMI_INT_BATCH_PROCESS>();
        }

        [Key]
        [StringLength(2)]
        public string INT_SOURCE_ID { get; set; }

        [Required]
        [StringLength(30)]
        public string INT_SOURCE_NAME { get; set; }

        [Required]
        [StringLength(30)]
        public string INT_NAME { get; set; }

        [Required]
        [StringLength(10)]
        public string MODUL { get; set; }

        [StringLength(100)]
        public string SOURCE_VALUE { get; set; }

        [StringLength(200)]
        public string PARAMETER1 { get; set; }

        [StringLength(200)]
        public string PARAMETER2 { get; set; }

        [StringLength(200)]
        public string PARAMETER3 { get; set; }

        [StringLength(200)]
        public string PARAMETER4 { get; set; }

        [StringLength(200)]
        public string PARAMETER5 { get; set; }

        [StringLength(100)]
        public string CREATED_BY { get; set; }

        public DateTime? CREATION_DATE { get; set; }

        [StringLength(100)]
        public string LAST_UPDATED_BY { get; set; }

        public DateTime? LAST_UPDATE_DATE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SMI_INT_BATCH_PROCESS> SMI_INT_BATCH_PROCESS { get; set; }
    }
}
