namespace SMI.MonitoringOracle
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("APPS.SMI_INT_VALIDATION_LOG")]
    public partial class SMI_INT_VALIDATION_LOG
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(2)]
        public string INT_SOURCE_ID { get; set; }

        [Key]
        [Column(Order = 1)]
        public decimal INT_BATCH_ID { get; set; }

        [Key]
        [Column(Order = 2)]
        public decimal INT_STAGING_ID { get; set; }

        [Key]
        [Column(Order = 3)]
        public decimal INT_LOG_SEQ { get; set; }

        [Required]
        [StringLength(4000)]
        public string VALIDATION_LOG { get; set; }

        [StringLength(100)]
        public string CREATED_BY { get; set; }

        public DateTime? CREATION_DATE { get; set; }

        [StringLength(100)]
        public string LAST_UPDATED_BY { get; set; }

        public DateTime? LAST_UPDATE_DATE { get; set; }

        public decimal? INT_REQUEST_ID { get; set; }
    }
}
